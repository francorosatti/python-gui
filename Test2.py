###############################################################################
# Name: Test2.py
# Desc: Formulario de prueba con Tkinter orientado a objetos
# Auth: Franco Rosatti
# Date: Dec-2019
# Vers: 1.0
###############################################################################

import tkinter as tk
from tkinter import ttk


class FormTest():

    def __init__(self):
        self.raiz = tk.Tk()
        self.raiz.geometry('300x200')
        self.raiz.configure(bg='white')
        self.raiz.title('Aplicación')
        ttk.Button(self.raiz, text='Salir',
                   command=self.raiz.destroy).pack(side=tk.BOTTOM)

    def ShowDialog(self):
        self.raiz.mainloop()
        return 2


def main():
    f = FormTest()
    if f.ShowDialog() == 1:
        print("ok")
    else:
        print("nok")

    return 0


if __name__ == '__main__':
    main()
