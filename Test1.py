###############################################################################
# Name: Test1.py
# Desc: Formulario de prueba con Tkinter
# Auth: Franco Rosatti
# Date: Dec-2019
# Vers: 1.0
###############################################################################

import tkinter as tk
from tkinter import ttk  # Carga ttk (para widgets nuevos 8.5+)

# Define la ventana principal de la aplicación

raiz = tk.Tk()

raiz.geometry('300x200')  # anchura x altura

raiz.configure(bg='white')

raiz.title('Aplicación')

ttk.Button(raiz, text='Salir', command=quit).pack(side=tk.BOTTOM)

raiz.mainloop()
